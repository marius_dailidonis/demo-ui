import React from 'react'
import axios from 'axios'

export default class CommentsList extends React.Component {
    state = {
        comments: [],
    }

    componentDidMount() {
        axios.get('http://localhost:8080/comments')
            .then(res => {
                const comments = res.data;
                this.setState({
                    comments
                });
            });
    }

    render() {
        return (
            <ul>
                {this.state.comments.map(comment => <li key={comment.id}>{comment.content}</li>)}
            </ul>
        );
    }
}